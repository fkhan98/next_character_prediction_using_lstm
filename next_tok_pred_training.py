from keras.models import Sequential
from keras.layers import Dense, Dropout, LSTM
from keras.optimizers import RMSprop
import numpy as np
import random
import sys
from keras.callbacks import ModelCheckpoint
from matplotlib import pyplot as plt

def load_file(filepath: str):
    raw_text = open(filepath, 'r', encoding='utf-8').read()
    raw_text = raw_text.lower()
    return raw_text

def preprocess_text(text: str):
    num_removed_text = ''.join(c for c in text if not c.isdigit())
    #specialchar_removed_text = ''.join(char for char in num_removed_text if char.isalnum())
    #return specialchar_removed_text
    return num_removed_text

def create_mapping_dictionary(text: str):
    chars = sorted(list(set(text)))
    #Create a dictionary of characters mapped to integer values
    char_to_int = dict((c, i) for i, c in enumerate(chars))
    #Create a dictionary of integer values mapped to characters
    int_to_char = dict((i, c) for i, c in enumerate(chars))

    return char_to_int,int_to_char,chars

def generate_sentence_and_next_char_list(text: str, seq_length: int, step: int):
    sentences = []    # X values (Sentences)
    next_chars = []   # Y values. The character that follows the sentence defined as X
    for i in range(0, n_chars - seq_length, step):  #step=1 means each sentence is offset just by a single letter
        sentences.append(preprocessed_txt[i: i + seq_length])  #Sequence in
        next_chars.append(preprocessed_txt[i + seq_length])  #Sequence out
    
    return sentences, next_chars

def prepare_dataset(sentences: str,next_char: list,seq_length: int, n_vocab: int, c_to_i: dict):
    x = np.zeros((len(sentences), seq_length, n_vocab), dtype=bool)
    y = np.zeros((len(sentences), n_vocab), dtype=bool)

    for i, sentence in enumerate(sentences):
        for t, char in enumerate(sentence):
            x[i, t, c_to_i[char]] = 1
        y[i, c_to_i[next_char[i]]] = 1
    
    return x,y

def visualize_results(history):
    #plot the training accuracy and loss at each epoch
    loss = history.history['loss']
    epochs = range(1, len(loss) + 1)
    plt.plot(epochs, loss, 'y', label='Training loss')
    plt.title('Training loss')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.show()

def get_model(seq_length, n_vocab):
    model = Sequential()
    model.add(LSTM(128, input_shape=(seq_length, n_vocab)))
    model.add(Dense(n_vocab, activation='softmax'))

    optimizer = RMSprop(lr=0.01)
    model.compile(loss='categorical_crossentropy', optimizer=optimizer)

    return model

if __name__ == '__main__':
    filename = "./1661-0.txt"
    unprocessed_file = load_file(filename)
    preprocessed_txt = preprocess_text(unprocessed_file)
    c_to_i, i_to_c,chars = create_mapping_dictionary(preprocessed_txt)
    # summarize the data
    n_chars = len(preprocessed_txt)
    n_vocab = len(chars)
    print("Total Characters in the text; corpus length: ", n_chars)
    print("Total Vocab: ", n_vocab)
    #print(len(unprocessed_file))
    seq_length = 60
    sentences, next_char = generate_sentence_and_next_char_list(preprocessed_txt, seq_length, 10)
    print(type(next_char))
    # print(sentences[10])
    # print(next_char[10])
    n_patterns = len(sentences)    
    print('Number of sequences:', n_patterns)

    x,y = prepare_dataset(sentences, next_char, seq_length, n_vocab, c_to_i)
    print(x.shape)
    print(y.shape)

    #initializing and compiling
    # model = Sequential()
    # model.add(LSTM(128, input_shape=(seq_length, n_vocab)))
    # model.add(Dense(n_vocab, activation='softmax'))

    # optimizer = RMSprop(lr=0.01)
    # model.compile(loss='categorical_crossentropy', optimizer=optimizer)

    model = get_model(seq_length, n_vocab)

    filepath="saved_weights/saved_weights-{epoch:02d}-{loss:.4f}.hdf5"
    checkpoint = ModelCheckpoint(filepath, monitor='loss', verbose=1, save_best_only=True, mode='min')
    callbacks_list = [checkpoint]


    # Fit the model

    history = model.fit(x, y,
            batch_size=128,
            epochs=50,   
            callbacks=callbacks_list)

    visualize_results(history)
    model.save('weights_ntp_20epochs.h5')