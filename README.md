# Next_character_prediction_using_LSTM

An LSTM was trained from scratch(using Keras) on Project Gutenberg's The Adventures of Sherlock Holmes to predict the next character from a sequence of 60 characters. After the model was trained it takes a randomly initialized 60 character to predict the next character. Then I use that output to keep on predicting next characters until we have an output of a size of 400 characters.

# The dataset to train the next character predictor model
I use the text file that is available in this link->(https://drive.google.com/file/d/1GeUzNVqiixXHnTl8oNiQ2W3CynX_lsu2/view). I remove some irrelevant information that are present in the initial parts of the text document. 

# Training the model
I prepare a dataset of sentences(X) of size(60 characters) and their corresponding next character(Y) and changed it such that an LSTM can learn from the dataset. The LSTM itself that was used a simple neural network with one LSTM layer of 128 hidden units followed by a densely connected layer with output size equal to the vocubulary of the dataset. Softmax activation was used so as to predict and caculate loss on which character was being predicted for an input sequence. The code to prepare the dataset and train the model is inside the next_tok_pred_training.py. 

# Predicting using the trained model
We use a randomly generated text sequence of 60 characters and use that as input to the trained model and predict the next character. We keep on forming input sequences of 60 characters and keep on predicting next character until we get an output of a size of 200 characters. The code for this prediction is inside prediction.py.









