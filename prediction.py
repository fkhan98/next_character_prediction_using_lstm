from keras.models import Sequential
from keras.layers import Dense, Dropout, LSTM
from keras.optimizers import RMSprop
import numpy as np
import random
import sys
from keras.callbacks import ModelCheckpoint
from matplotlib import pyplot as plt


def sample(preds):
    preds = np.asarray(preds).astype('float64')
    preds = np.log(preds)
    exp_preds = np.exp(preds) #exp of log (x), isn't this same as x??
    preds = exp_preds / np.sum(exp_preds)
    probas = np.random.multinomial(1, preds, 1) 
    return np.argmax(probas)

def load_file(filepath: str):
    raw_text = open(filepath, 'r', encoding='utf-8').read()
    raw_text = raw_text.lower()
    return raw_text

def preprocess_text(text: str):
    num_removed_text = ''.join(c for c in text if not c.isdigit())
    #specialchar_removed_text = ''.join(char for char in num_removed_text if char.isalnum())
    #return specialchar_removed_text
    return num_removed_text

def create_mapping_dictionary(text: str):
    chars = sorted(list(set(text)))
    #Create a dictionary of characters mapped to integer values
    char_to_int = dict((c, i) for i, c in enumerate(chars))
    #Create a dictionary of integer values mapped to characters
    int_to_char = dict((i, c) for i, c in enumerate(chars))

    return char_to_int,int_to_char,chars

def get_model(seq_length: int, n_vocab: int):
    model = Sequential()
    model.add(LSTM(128, input_shape=(seq_length, n_vocab)))
    model.add(Dense(n_vocab, activation='softmax'))

    optimizer = RMSprop(lr=0.01)
    model.compile(loss='categorical_crossentropy', optimizer=optimizer)

    return model

if __name__ == '__main__':
    filename = "./1661-0.txt"
    unprocessed_file = load_file(filename)
    preprocessed_txt = preprocess_text(unprocessed_file)
    c_to_i, i_to_c,chars = create_mapping_dictionary(preprocessed_txt)

    seq_length = 60
    n_vocab = len(chars)
    n_chars = len(preprocessed_txt)

    model = get_model(seq_length, n_vocab)
    model.load_weights("./weights_ntp_50epochs.h5")

    #Initiate generated text and keep adding new predictions and print them out
    start_index = random.randint(0, n_chars - seq_length - 1)
    generated = ''
    sentence = preprocessed_txt[start_index: start_index + seq_length]
    generated += sentence
    print('----- Seed for our text prediction: "' + sentence + '"')
    for i in range(400):   # Number of characters including spaces
        x_pred = np.zeros((1, seq_length, n_vocab))
        for t, char in enumerate(sentence):
            x_pred[0, t, c_to_i[char]] = 1.

        preds = model.predict(x_pred, verbose=0)[0]
        next_index = sample(preds)
        next_char = i_to_c[next_index]

        generated += next_char
        sentence = sentence[1:] + next_char

        sys.stdout.write(next_char)
        sys.stdout.flush()
    print()


